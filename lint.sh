#!/bin/sh

python manage.py makemigrations --dry-run --check
find ./ -type f -name "*.py" | grep -v "./env/" | xargs pylint
python manage.py lint
