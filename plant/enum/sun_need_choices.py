from django.db.models import IntegerChoices
from django.utils.translation import gettext_lazy as _


class SunNeedChoices(IntegerChoices):
    NO_SUN = 0, _('No sunlight needed')
    LITTLE_SUN = 1, _('Little sunlight needed')
    MODERATE_SUN = 2, _('Moderate sunlight needed')
    HIGH_SUN = 3, _('High sunlight needed')
