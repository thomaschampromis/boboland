from django.db.models import IntegerChoices
from django.utils.translation import gettext_lazy as _

class ColorChoices(IntegerChoices):
    RED = 1, _('Red')
    ORANGE = 2, _('Orange')
    YELLOW = 3, _('Yellow')
    GREEN = 4, _('Green')
    BLUE = 5, _('Blue')
    PURPLE = 6, _('Purple')


