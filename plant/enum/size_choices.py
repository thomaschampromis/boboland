from django.db.models import IntegerChoices
from django.utils.translation import gettext_lazy as _

class SizeChoices(IntegerChoices):
    SMALL = 1, _('Small')
    MIDDLE = 2, _('Middle')
    TALL = 3, _('Tall')
    VERY_TALL = 4, _('Very tall')
