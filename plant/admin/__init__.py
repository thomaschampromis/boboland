'''
init admin plant
'''

from .house import HouseAdmin
from .plant import PlantAdmin
from .room import RoomAdmin