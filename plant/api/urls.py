from django.urls import path
from .views import PlantView

urlpatterns = [
    path('plant/', PlantView.as_view(), name='plant_list'),
]