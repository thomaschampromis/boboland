from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.generics import ListAPIView
from rest_framework.response import Response




from plant.api.serializers import PlantSerializer
from ..models import Plant
# pylint: disable=invalid-name


class PlantView(ListAPIView):
    '''
    api get plantes
    '''
    serializer_class = PlantSerializer

    permission_classes = [IsAuthenticated]
    
    # pylint: disable=unused-argument
    def get(self, request, **_):
        '''
        récupérer une Plant
        '''

        queryset = Plant.objects.filter(room__house__owner=request.user)
        
        return Response({queryset})



