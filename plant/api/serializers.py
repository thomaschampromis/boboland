'''
    Serializer des données de l'api PLant
'''
from rest_framework import serializers
from ..models import Plant, Room, House
# pylint: disable=invalid-name


class PlantSerializer(serializers.ModelSerializer):
    '''
    On retourne les informations d'une plant
    '''

    class Meta:
        '''
        Meta du serializer Plant
        '''

        
        model = Plant
        fields = [
            'id',
            'category',
        ]
    
    
    
class PlantCardSerializer(serializers.ModelSerializer):
    '''
    On retourne les informations d'une plant
    '''

    class Meta:
        '''
        Meta du serializer Plant
        '''

        model = Plant
        fields = [
            'id',
            'name', 
            'size', 
            'last_water', 
            'need_sun',
        ]
    



class RoomSerializer(serializers.ModelSerializer):
    '''
    On retourne les informations d'une room
    '''

    class Meta:
        '''
        Meta du serializer room
        '''

        
        model = Room
        fields = [
            'id',
            'name',
            'color',
        ]
    
    