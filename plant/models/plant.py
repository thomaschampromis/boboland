'''
Modele Plant
'''
from django.conf import settings
from django.db import models
from django.db.models import UniqueConstraint
from django.utils.translation import gettext_lazy as _
from django.core.exceptions import ValidationError

from plant.models.room import Room
from plant.models.category import Category
from ..enum import ColorChoices, SunNeedChoices, SizeChoices

# pylint: disable=invalid-name
class Plant(models.Model):
    '''
    model Plant
    '''

   
    name = models.CharField(
        max_length=50,
        verbose_name=_('name'),
    )

    description = models.CharField(
        max_length=400,
        verbose_name=_("Description"),
        default='',
        blank=True,
    )
    
    color = models.IntegerField(
        choices = ColorChoices.choices,
        verbose_name=_("Description"),
        default=ColorChoices.GREEN
    )
    
    stamina = models.PositiveSmallIntegerField(
        default=4,
        verbose_name=_('stamina'),
    )
    
    last_water = models.DateTimeField(
        auto_now=True,
        verbose_name=_('Date last water'),
    )
    
    need_sun = models.IntegerField(
        choices = SunNeedChoices.choices,
        verbose_name=_("need sun"),
        default=SunNeedChoices.MODERATE_SUN
    )
    
    arrived_at = models.DateTimeField(
        auto_created=True,
        verbose_name=_('Date arrived in appartment'),
    )
    count_water = models.PositiveIntegerField(
        verbose_name=_('count water')
    )
    
    size = models.IntegerField(
        choices = SizeChoices.choices,
        verbose_name=_("size"),
        default=SizeChoices.MIDDLE,
    )
    
    room = models.ForeignKey(
        Room, 
        verbose_name=_("related room"), 
        on_delete=models.CASCADE,
    )
    
    category = models.ForeignKey(
        Category,
        on_delete=models.DO_NOTHING,
        verbose_name=_("category"),
        null=True,
        blank=True, 
    )
    
    position_x = models.FloatField(
        null=True,
        verbose_name=_('position_x'),
    )

    position_y = models.FloatField(
        null=True,
        verbose_name=_('position_y'),
    )
    
    
    def __str__(self):
        return self.name

    def clean(self):
        '''
        Vérification des champs
        '''

    class Meta:
        '''
        Meta des plantes
        '''
        verbose_name = _('Plant')