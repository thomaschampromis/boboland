'''
Modele Category
'''
from django.db import models
from django.utils.translation import gettext_lazy as _


# pylint: disable=invalid-name
class Category(models.Model):
    '''
    model Category
    '''

   
    name = models.CharField(
        max_length=50,
        verbose_name=_('name'),
    )
    
    icon = models.ImageField(
        verbose_name=_('icon')
    )
    
    description = models.CharField(
        max_length=400,
        verbose_name=_("Description"),
        default='',
        blank=True,
    )
    
    
    def __str__(self):
        return self.name

    def clean(self):
        '''
        Vérification des champs
        '''

    class Meta:
        '''
        Meta des catégories
        '''
        verbose_name = _('Category')