'''
Modele Plant
'''
from django.conf import settings
from django.db import models
from django.db.models import UniqueConstraint
from django.utils.translation import gettext_lazy as _
from django.core.exceptions import ValidationError
from django.contrib.auth.models import User

from ..enum import ColorChoices, SunNeedChoices, SizeChoices

# pylint: disable=invalid-name
class House(models.Model):
    '''
    model House
    '''

   
    name = models.CharField(
        max_length=50,
        verbose_name=_('name'),
    )
    
    owner = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
    )
    
    
    
    def __str__(self):
        return self.name

    def clean(self):
        '''
        Vérification des champs
        '''

    class Meta:
        '''
        Meta des maisons
        '''
        verbose_name = _('House')