'''
Modele Plant
'''
from django.conf import settings
from django.db import models
from django.utils.translation import gettext_lazy as _

from plant.models.house import House
from ..enum import ColorChoices
# pylint: disable=invalid-name
class Room(models.Model):
    '''
    model Room
    '''

   
    name = models.CharField(
        max_length=50,
        verbose_name=_('name'),
    )
 
    color = models.IntegerField(
        choices = ColorChoices.choices,
        verbose_name=_("Description"),
        default=ColorChoices.BLUE
    )
    
    house = models.ForeignKey(
        House, 
        verbose_name=_("related house"), 
        on_delete=models.CASCADE,
    )
    
    
    def __str__(self):
        return self.name

    def clean(self):
        '''
        Vérification des champs
        '''

    class Meta:
        '''
        Meta des pièces
        '''
        verbose_name = _('Room')